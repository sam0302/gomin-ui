// Include Desktop Specific JavaScript files here (or inside of your Desktop Controller, or differentiate based off App.mobile === false)
require(["App", "routers/AppRouter", "service/AppService","controllers/DesktopController", "jquery", "backbone", "marionette", "jqueryui", "bootstrap", "backbone.validateAll"
    ],
    function (App, AppRouter,AppService, AppController) {
        App.appRouter = new AppRouter({
            controller:new AppController()
        });
        App.addInitializer(function(ops) {
            AppService.init();
        });
        // Start Marionette Application in desktop mode (default)
        App.start();

    });