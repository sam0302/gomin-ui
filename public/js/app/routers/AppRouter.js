define(['backbone', 'marionette'], function(Backbone, Marionette) {
   return Backbone.Marionette.AppRouter.extend({
       //"index" must be a method in AppRouter's controller
       appRoutes: {
           "": "index",
           "firstPage" : "start",
           "secondPage": "secondPage",
           "thirdPage": "thirdPage"

       }
   });
});