define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/MobileWelcome','jquery.fullPage','bootstrap-slider','daterangepicker'],
    function(App, Backbone, Marionette, $, Model, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            template: template,
            model: new Model({
                mobile: App.mobile
            }),

            initialize: function() {
                _.bindAll(this);
            },

            // View Event Handlers
            events: {
               // 'change #isReturn' : "dateCheckBoxChanged"
            },

            onShow: function(){
               // $('#menu').hide();
                $('#fullpage').fullpage({
                    anchors: ['firstPage', 'secondPage', "thirdPage"],
                    slidesNavigation: true,
                    navigation: true,
                    fixedElements: '#fshare',
                    menu: '#menu',
                    continuousVertical: true,
                    autoScrolling:false
                });
                $('#startDate').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true
                });
            }
        });
    });