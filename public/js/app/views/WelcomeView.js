define( ['App', 'backbone', 'marionette', 'jquery', 'models/Model', 'hbs!templates/welcome','jquery.fullPage','bootstrap-slider','daterangepicker','backbone.stickit'],
    function(App, Backbone, Marionette, $, Model, template) {
        //ItemView provides some default rendering logic
        return Backbone.Marionette.ItemView.extend( {
            template: template,
            fullpage: "#fullpage",
            model: new Model({
                mobile: App.mobile
            }),

            initialize: function() {
                _.bindAll(this);
            },

            // View Event Handlers
            events: {
                'change #isReturn' : "dateCheckBoxChanged",
                'click #startSearch' : "startSearch"
            },

            bindings: {
                "#email" : {
                    observe: 'email'
                },
                "source" :{
                    observe: 'source'
                },
                "#destination":{
                    observe: 'destination'
                },
                "rangePrimary":{
                    observe: 'budget'
                },
                "#isUrgent":{
                    observe: "checkCancellation"
                },
                "#isHoteleRequired":{
                    observe: 'hotelNeeded'
                }
            },

            onShow: function(){
                this.stickit();
                $('#fullpage').fullpage({
                    anchors: ['firstPage', 'secondPage',"thirdPage"],
                    slidesNavigation: true,
                    navigation: true,
                    fixedElements: '#fshare',
                    menu: '#menu',
                    continuousVertical: true
                });
                $('#startDate').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true
                });
            },

            dateCheckBoxChanged: function(){
                if($('#isReturn').is(":checked")) {
                    // Round trip selected
                    $('#startDate').daterangepicker();
                }else{
                    $('#startDate').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true
                    });
                }
            },

            startSearch: function(){
                var result = App.request("details:search", this.model);
                if(result){
                    alert("Sit back and relax as we hunt the best deal for you.");
                }else{
                    alert("Oops! Seems like ther is a problem. We are working on fixing it.");
                }
            }
        });
    });