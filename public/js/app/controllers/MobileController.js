define(['App', 'backbone', 'marionette', 'views/MobileWelcomeView', 'views/MobileHeaderView','jquery.fullPage'],
    function (App, Backbone, Marionette, WelcomeView, MobileHeaderView) {
    return Backbone.Marionette.Controller.extend({
        initialize:function (options) {
            //App.headerRegion.show(new MobileHeaderView());
        },
        //gets mapped to in AppRouter's appRoutes
        index:function () {
            App.mainRegion.show(new WelcomeView());
        },
        start: function(){
            App.mainRegion.show(new WelcomeView());
        },

        secondPage: function(){
            App.mainRegion.show(new WelcomeView());
        },

        thirdPage: function(){
            App.mainRegion.show(new WelcomeView());
        }
    });
});