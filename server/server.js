
// DEPENDENCIES
// ============
var express = require("express"),
    http = require("http"),
    port = (process.env.PORT || 8080),
    server = module.exports = express();

var mongoose = require('mongoose');


var passport = require('passport');
require('passport-local');
var flash    = require('connect-flash');
var path = require('path'),
    fs = require('fs');



var configDB = require('../config/database.js');

mongoose.connect(configDB.url);



require('../config/passport')(passport);
// SERVER CONFIGURATION
// ====================
server.configure(function () {

    server.use(express["static"](__dirname + "/../public"));

    server.use(express.errorHandler({

        dumpExceptions:true,

        showStack:true

    }));





  //  app.use(express.static(path.join(__dirname, 'public')));
    server.set('views', __dirname + '/views');
    server.engine('html', require('ejs').renderFile);

    server.use(express.cookieParser());
    server.use(express.bodyParser());
    server.use(express.bodyParser({uploadDir:'/images'}));
    server.use(express.session({ secret: 'knoldus' }));
    server.use(passport.initialize());
    server.use(passport.session());
    server.use(server.router);
    server.use(flash());
});


// SERVER
// ======
require('../app/routes.js')(app, passport,server);



// Start Node.js Server
var app =http.createServer(server).listen(port);

console.log('Welcome to Marionette-Require-Boilerplate!\n\nPlease go to http://localhost:' +server.name+ port + ' to start using Require.js and Marionette');